import { Component, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/Pokemon';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent{

  inputValue: string = "";
  isDisabled: boolean = true;
  pokemons: Pokemon[];
  
  constructor() { 

    this.pokemons = [
      {id: 1 , name: 'Bulbasaur',  type: 'Grass/Poison' },
      {id: 2 , name: 'IvySaur',    type : 'Grass/Poison'},
      {id: 3 , name: 'Venusaur',   type: 'Grass/Poison'},
      {id: 4 , name: 'Charmender', type: 'Fire'},
      {id: 5 , name: 'Charmeleon', type: 'Fire/Flying'},
      {id: 6 , name: 'Charizard',  type: 'Fire'},
      {id: 7 , name: 'Squirtle',   type : 'Water'},
      {id: 8 , name: 'Warturtle',  type : 'Water'},
      {id: 9 , name: 'Blastoise',  type: 'Water'},
      {id:10 , name: "Ditto",      type:"Normal"},
      {id:11 , name: "Arceus",     type:"Normal"},
    ];
  }

  searchPokemonInList(): void {
    this.pokemons.filter(p => p.name.toLowerCase() === this.inputValue.toLowerCase()).length > 0 ? console.log("Trovato") : console.log("Datte foco");
  }

  keyPressInput($event: any): void{
    this.inputValue = ($event.target as HTMLInputElement).value;
    console.log(this.inputValue);

    this.isDisabled = this.inputValue.length <2
  }


}