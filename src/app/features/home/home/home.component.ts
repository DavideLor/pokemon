import { Component, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/Pokemon';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  pokemons: Pokemon[]; 
  loading: boolean = true;
  
  constructor() { 

    setTimeout(() => {
    this.loading = false;
    }, 4000);


    this.pokemons = [
      {id: 1 , name: 'Bulbasaur' , type: 'Grass/Poison' },
      {id: 2 , name : 'IvySaur', type : 'Grass/Poison'},
      {id: 3 , name: 'Venusaur', type: 'Grass/Poison'},
      {id: 4 , name: 'Charmender' , type: 'Fire'},
      {id: 5 , name: 'Charmeleon' , type: 'Fire/Flying'},
      {id: 6 , name: 'Charizard', type: 'Fire'},
      {id: 7 , name: 'Squirtle', type : 'Water'},
      {id: 8 , name : 'Warturtle' , type : 'Water'},
      {id: 9 , name: 'Blastoise' , type: 'Water'},
      {id:10 , name:"Ditto", type:"Normal"},
      {id:11 , name:"Arceus", type:"Normal"},
    ]

  }

  ngOnInit(): void {
  }



}
